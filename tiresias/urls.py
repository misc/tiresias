from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index_form,),
    url(r'^show$', views.redirect_show_form, name='show_person'),
    url(r'^show/(?P<kerberos_id>(.+))$', views.show_person,),
]
