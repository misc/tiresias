from django.shortcuts import render, get_object_or_404
from .forms import SearchForm 
from django.http import HttpResponseRedirect, HttpResponseNotFound
from .models import get_canonical_kerberos_id, Person

def index_form(request):
    search_form = SearchForm()
    return render(request, 'index_form.html', {'search_form': search_form})

def redirect_show_form(request):
    k = request.GET.get('kerberos_id')
    if k:
        kerberos_id = get_canonical_kerberos_id(k)
        return HttpResponseRedirect(request.path+'/'+kerberos_id)
        # redirect vers la page 
    return HttpResponseNotFound()    
        # redirect vers index


def show_person(request, kerberos_id):
    person = get_object_or_404(Person, kerberos_id=kerberos_id)
    return render(request, 'show_person.html', {'person': person})

def show_faq(request):
    return render(request, 'faq.html', {})

