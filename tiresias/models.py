from django.db import models
from django.utils import timezone


def get_canonical_kerberos_id(kerberos_id):
    # TODO search mail alias
    if '@' in kerberos_id:
        kerberos_id = kerberos_id.split('@')[0]
    return kerberos_id.lower()

class Person(models.Model):
    kerberos_id = models.CharField(max_length=20)
    last_change_date = models.DateTimeField(
            default=timezone.now)
    creation_date = models.DateTimeField(
            default=timezone.now)
    pronouns = models.CharField(default='they/them', max_length=40)

    def __str__(self):
        return self.kerberos_id

    def display_pronouns(self):
        return '/'.join([p.capitalize() for p in self.pronouns.split('/')])

# Create your models here.
